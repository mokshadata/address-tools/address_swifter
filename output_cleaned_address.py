import csv
import os
import json

from address_swifter import clean_address

from dotenv import load_dotenv
load_dotenv()

source_file_path = r'data/short.csv'
geocode_output_file_path = r'outputs/xxx_geocodes.json'
formatted_output_file_path = r'outputs/xxx_formatted.csv'
output_error_file_path = r'outputs/xxx_errors.csv'
results_output_file_path = r'outputs/xxx_results.json'

transformations = ['check_empty', 'check_no_street_number', 'pre_cleaning_spaces', 'add_space_before_street_name']
bounds = {"north": 30.166256, "east": -94.920227, "south": 29.490625, "west": -95.962188}

def write_to_file(corrected_addresses): 
  with open(results_output_file_path, 'a', newline='', encoding='utf-8') as jsonf:
            jsonf.write(json.dumps(corrected_addresses, indent=4)) 
  for (key, value) in corrected_addresses.items():
    if key == "results":
      if isinstance(value, str):
        write_errors([value])
      else:
        with open(geocode_output_file_path, 'a', newline='', encoding='utf-8') as jsonf:
            jsonf.write(json.dumps(value, indent=4)) 

def write_errors(error_addresses):
  with open(output_error_file_path, 'a', newline='') as file:
    writer = csv.writer(file, delimiter=' ', quoting=csv.QUOTE_ALL)
    writer.writerows([error_addresses])


def process_csv(source_file_path):
  if os.path.exists(geocode_output_file_path):
    os.remove(geocode_output_file_path)
    print(geocode_output_file_path + 'output_file_path was removed')

  if os.path.exists(results_output_file_path):
    os.remove(results_output_file_path)
    print(results_output_file_path + 'results_output_file_path was removed')

  if os.path.exists(formatted_output_file_path):
    os.remove(formatted_output_file_path)
    print(formatted_output_file_path + 'formatted_output_file_path was removed')

  if os.path.exists(output_error_file_path):
    os.remove(output_error_file_path)
    print(output_error_file_path + 'output_file_path was removed')

  with open(source_file_path,  newline='\n', encoding='utf-8') as csvfile:
    addresses  =  csv.reader(csvfile, delimiter=' ', quotechar='"')
    for row in addresses:
      address = ' '.join(row)
      result = clean_address(address,  transformations = transformations, bounds= bounds, callback = write_to_file)

process_csv(source_file_path)