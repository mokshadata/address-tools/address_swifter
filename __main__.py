from address_swifter.cleaner import transforms
from address_swifter.cleaner import process_address
from address_swifter.cleaner import flags
from address_swifter.cleaner import split_to_parts
from address_swifter import swifter

# from dotenv import load_dotenv
# load_dotenv()

if __name__ == "__main__":
    import doctest
    doctest.testmod(swifter)
    doctest.testmod(transforms)
    doctest.testmod(process_address)
    doctest.testmod(flags)
    doctest.testmod(split_to_parts)


