from address_swifter.cleaner import transforms
from address_swifter.cleaner import flags
from address_swifter.cleaner import process_address
from address_swifter.utils import quick_dict_serialize
from cachetools import LRUCache, cached
from cachetools.keys import hashkey

DEFAULT_TRANSFORMS = ['check_empty', 'check_no_street_number', 'pre_cleaning_spaces', 'add_space_before_street_name']


def make_key_for_clean_address(*args, transformations = [], bounds = [], geocoder_client_options = {}, **kwargs):
    """
      Takes the lists of transformations, bounds and geocoder client's opetions and converts each one of them to tuples
      
      :param transformations: The list of tranformations for the string, defaults to ['check_empty', 'check_no_street_number', 'pre_cleaning_spaces', 'add_space_before_street_name']
      :type transformations: list
      :param bounds: The list of tranformations for the string, defaults to []
      :type bounds: list
      :param geocoder_client_options: The dictiona of the geocoder client's options, defaults to {}
      :type geocoder_client_options: dictionary

      
      >>> make_key_for_clean_address(DEFAULT_TRANSFORMS, {"north": 30.166256, "east": -94.920227, "south": 29.490625, "west": -95.962188})
      (['check_empty', 'check_no_street_number', 'pre_cleaning_spaces', 'add_space_before_street_name'], {'north': 30.166256, 'east': -94.920227, 'south': 29.490625, 'west': -95.962188}, '{}', '{}')

    """


    key = hashkey(*args, **kwargs)
    key += tuple(transformations) + tuple([quick_dict_serialize(bounds or {})]) + tuple([quick_dict_serialize(geocoder_client_options or {})])
    return key


def normalize_geocode_response(geocode_response, fallback_address_string):
  geocode_results = []
  geocode_errors = []

  if geocode_response.get('error_message'):
    geocode_errors = [geocode_response.get('error_message')]
    geocode_results = [{"formatted_address": fallback_address_string}]
  else:
    geocode_results = geocode_response.get('results')

  return geocode_results, geocode_errors

@cached(LRUCache(maxsize=128), key=make_key_for_clean_address)
def clean_address(address_string, transformations = DEFAULT_TRANSFORMS, google_key = None, bounds = [], verbose = False, callback = None, geocoder_client_options = {}):
  """
    The main function that assembles the various instructions (address string, transformations, bounds, etc.) into the final desired output based on the parameters passed.
    
    :param address_string: the address string to be cleaned
    :type address_string: string
    :param transformations: The list of tranformations for the string, defaults to ['check_empty', 'check_no_street_number', 'pre_cleaning_spaces', 'add_space_before_street_name']
    :type transformations: list
    :param google_key: the google key stored in .env or provided
    :type google_key: string
    :param bounds: The list of tranformations for the string, defaults to []
    :type bounds: list
    :param verbose: the address string to be cleaned
    :type verbose: Boolean
    :param callback: A callback function to put the output to use, defaults to None
    :type callback: function
    
    >>> clean_address("19311 CYPRESS PEAK LANE KATY TX USA", verbose=True)
    {'input_string': '19311 CYPRESS PEAK LANE KATY TX USA', 'prepped_address': '19311 CYPRESS PEAK LANE KATY TX USA', 'results': [{'address_components': [{'long_name': '19311', 'short_name': '19311', 'types': ['street_number']}, {'long_name': 'Cypress Peak Lane', 'short_name': 'Cypress Peak Ln', 'types': ['route']}, {'long_name': 'Cypress Meadows', 'short_name': 'Cypress Meadows', 'types': ['neighborhood', 'political']}, {'long_name': 'Katy', 'short_name': 'Katy', 'types': ['locality', 'political']}, {'long_name': 'Harris County', 'short_name': 'Harris County', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Texas', 'short_name': 'TX', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'United States', 'short_name': 'US', 'types': ['country', 'political']}, {'long_name': '77449', 'short_name': '77449', 'types': ['postal_code']}, {'long_name': '4102', 'short_name': '4102', 'types': ['postal_code_suffix']}], 'formatted_address': '19311 Cypress Peak Ln, Katy, TX 77449, USA', 'geometry': {'bounds': {'northeast': {'lat': 29.8402578, 'lng': -95.7066444}, 'southwest': {'lat': 29.8400786, 'lng': -95.7068152}}, 'location': {'lat': 29.8401701, 'lng': -95.7067242}, 'location_type': 'ROOFTOP', 'viewport': {'northeast': {'lat': 29.8415171802915, 'lng': -95.7053808197085}, 'southwest': {'lat': 29.8388192197085, 'lng': -95.7080787802915}}}, 'place_id': 'ChIJB9Wh0CLYQIYRoqccrk2qgSU', 'types': ['premise']}], 'formatted_results': [{'address': '19311 Cypress Peak Ln, Katy, TX 77449, USA', 'parts': OrderedDict([('address1', '19311 Cypress Peak Ln'), ('city', 'Katy'), ('state', 'TX'), ('zip_code', '77449'), ('country', 'USA')]), 'type': 'Street Address', 'extra_parts': {}, 'detailed_parts': OrderedDict([('AddressNumber', '19311'), ('StreetName', 'Cypress Peak'), ('StreetNamePostType', 'Ln'), ('PlaceName', 'Katy'), ('StateName', 'TX'), ('ZipCode', '77449'), ('CountryName', 'USA')]), 'details_score': 0.8714285714285714}], 'errors': [], 'flags_raised': [], 'transforms': ['check_empty', 'check_no_street_number', 'pre_cleaning_spaces', 'add_space_before_street_name']}

    >>> clean_address("19311 CYPRESS PEAK LANE KATY TX USA")
    {'input_string': '19311 CYPRESS PEAK LANE KATY TX USA', 'prepped_address': '19311 CYPRESS PEAK LANE KATY TX USA', 'formatted_address': '19311 Cypress Peak Ln, Katy, TX 77449, USA', 'address_components': OrderedDict([('address1', '19311 Cypress Peak Ln'), ('city', 'Katy'), ('state', 'TX'), ('zip_code', '77449'), ('country', 'USA')]), 'latitude': 29.8401701, 'longitude': -95.7067242, 'location_type': 'Street Address', 'errors': [], 'flags_raised': []}

  """
  flags_raised = []
  
  transformed_address = transforms.handle_tranforms(address_string, transformations)

  if (transformed_address != None):
    occupancy_parts = process_address.get_occupancy_parts(transformed_address)
    prepped_address, details_score = process_address.rebuild_address(transformed_address)
    geocode_response = process_address.geocode_results(prepped_address, google_key = google_key, bounds = bounds, geocoder_client_options = geocoder_client_options)
    
    geocode_results, geocode_errors = normalize_geocode_response(geocode_response, address_string)

    if details_score < 0.5 or geocode_response.get('error_message'):
      prepped_address = transformed_address
      occupancy_parts = {}
      geocode_without_prep_response = process_address.geocode_results(transformed_address, google_key = google_key, bounds = bounds, geocoder_client_options = geocoder_client_options)

      geocode_results, geocode_errors = normalize_geocode_response(geocode_without_prep_response, address_string)

    if flags.is_partial_result(geocode_results) == True:
      flags_raised.append("Geocode partial match only")
    
    #simplified results
    for geo_result in geocode_results:
      address = process_address.rebuild_address_string(geo_result, **occupancy_parts)
      formatted_address = process_address.rebuild_address_string(geo_result, **occupancy_parts)
      address_parts, address_type, extra_parts, detailed_parts, details_score = process_address.convert_address_to_parts(address)
      
      if geo_result.get('geometry'):
        location = geo_result.get('geometry').get('location')
        latidude = location.get('lat')
        longitude = location.get('lng')
      else: 
        latidude = "N/A"
        longitude = "N/A"
    
    formatted_results = [
      process_address.rebuild_address_with_parts(geo_result, **occupancy_parts)
      for geo_result in geocode_results
    ]
    
    if flags.is_ambiguous_result(formatted_results) == True:
      flags_raised.append("Ambiguous result")
    
    if verbose == True:
      response = {
        'input_string': address_string,
        'prepped_address': prepped_address,
        'results': geocode_results,
        'formatted_results': formatted_results,
        'errors': geocode_errors,
        'flags_raised': flags_raised,
        'transforms': transformations,
      }
    else:
      response = {
        'input_string':address_string,
        'prepped_address': prepped_address,
        'formatted_address': formatted_address,
        'address_components':address_parts,
        'latitude': latidude,
        'longitude': longitude,
        'location_type':address_type,
        'errors':geocode_errors,
        'flags_raised':flags_raised
      }
    # print(json.dumps(response))
    if callback is not None:
        callback(response)
    return response