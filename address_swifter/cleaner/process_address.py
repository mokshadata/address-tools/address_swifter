from address_swifter.geocoders import googlemaps
from address_swifter.cleaner.split_to_parts import convert_address_to_parts, MAPPED_TAGS

# TODO finish this
# def parts_to_dict(address_parts, callback ):
#     for (key, value) in address_parts.items():
#       if parts_to_dict.get(key) is None:
#         parts_to_dict[key] = value
#         part =  parts_to_dict.get(key)

#         if part:
#           callback

def get_occupancy_parts(address):
  """
    Break down address string into parts, return only the occupancy type and identifier as a dict
    
    >>> get_occupancy_parts("19311 CYPRESS unit 4 PEAK LANE KATY TX USA")
    {'address2': 'unit 4'}

  """

  parts, address_type, extra_parts, detailed_parts, details_score = convert_address_to_parts(address)
  address_parts  = parts

  parts_to_dict = {}
  occupancy_parts ={}
  for (key, value) in address_parts.items():
    if parts_to_dict.get(key) is None:
      parts_to_dict[key] = value
      part =  parts_to_dict.get(key)

      if part:
        if  parts_to_dict.get("address2"):
          occupancy_parts["address2"] = parts_to_dict.get("address2")
  return occupancy_parts

def rebuild_address(address):
  """
    Rebuild the address string without the occupancy parts
    
    >>> rebuild_address("19311 CYPRESS unit 4 PEAK LANE KATY TX USA")
    ('19311 CYPRESS PEAK LANE KATY TX USA', 0.6)

  """
  address_parts, address_type, extra_parts, detailed_parts, details_score = convert_address_to_parts(address)

  parts_to_dict = {}
  parts_list = []
  recomposed_address = ""

  for (key, value) in address_parts.items():
    if parts_to_dict.get(key) is None:
      parts_to_dict[key] = value
      part = parts_to_dict.get(key)
      
      if part and  part != parts_to_dict.get("address2") :
        parts_list.append(part) 
        recomposed_address = " ".join(parts_list)

  return recomposed_address, details_score


def geocode_results(address, google_key = None, bounds = None, geocoder_client_options = {}):
  """
    Process the string through googlemaps geocode
    
    >>> geocode_results("19311 CYPRESS PEAK LANE KATY TX USA")
    {'results': [{'address_components': [{'long_name': '19311', 'short_name': '19311', 'types': ['street_number']}, {'long_name': 'Cypress Peak Lane', 'short_name': 'Cypress Peak Ln', 'types': ['route']}, {'long_name': 'Cypress Meadows', 'short_name': 'Cypress Meadows', 'types': ['neighborhood', 'political']}, {'long_name': 'Katy', 'short_name': 'Katy', 'types': ['locality', 'political']}, {'long_name': 'Harris County', 'short_name': 'Harris County', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Texas', 'short_name': 'TX', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'United States', 'short_name': 'US', 'types': ['country', 'political']}, {'long_name': '77449', 'short_name': '77449', 'types': ['postal_code']}, {'long_name': '4102', 'short_name': '4102', 'types': ['postal_code_suffix']}], 'formatted_address': '19311 Cypress Peak Ln, Katy, TX 77449, USA', 'geometry': {'bounds': {'northeast': {'lat': 29.8402578, 'lng': -95.7066444}, 'southwest': {'lat': 29.8400786, 'lng': -95.7068152}}, 'location': {'lat': 29.8401701, 'lng': -95.7067242}, 'location_type': 'ROOFTOP', 'viewport': {'northeast': {'lat': 29.8415171802915, 'lng': -95.7053808197085}, 'southwest': {'lat': 29.8388192197085, 'lng': -95.7080787802915}}}, 'place_id': 'ChIJB9Wh0CLYQIYRoqccrk2qgSU', 'types': ['premise']}], 'status': 'OK'}

  """
  return googlemaps.geocode(address, google_key = google_key, bounds = bounds, geocoder_client_options = geocoder_client_options)

def rebuild_address_string(geo_result, **occupancy_parts):
  """
    Reassemble the formatted addressed from the geocoder and the occupancy parts, following the MAPPED_TAGS order 
    
    >>> rebuild_address_string({'address_components': [{'long_name': '19311', 'short_name': '19311', 'types': ['street_number']}, {'long_name': 'Cypress Peak Lane', 'short_name': 'Cypress Peak Ln', 'types': ['route']}, {'long_name': 'Cypress Meadows', 'short_name': 'Cypress Meadows', 'types': ['neighborhood', 'political']}, {'long_name': 'Katy', 'short_name': 'Katy', 'types': ['locality', 'political']}, {'long_name': 'Harris County', 'short_name': 'Harris County', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Texas', 'short_name': 'TX', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'United States', 'short_name': 'US', 'types': ['country', 'political']}, {'long_name': '77449', 'short_name': '77449', 'types': ['postal_code']}, {'long_name': '4102', 'short_name': '4102', 'types': ['postal_code_suffix']}], 'formatted_address': '19311 Cypress Peak Ln, Katy, TX 77449, USA', 'geometry': {'bounds': {'northeast': {'lat': 29.8402578, 'lng': -95.7066444}, 'southwest': {'lat': 29.8400786, 'lng': -95.7068152}}, 'location': {'lat': 29.8401701, 'lng': -95.7067242}, 'location_type': 'ROOFTOP', 'viewport': {'northeast': {'lat': 29.8415171802915, 'lng': -95.7053808197085}, 'southwest': {'lat': 29.8388192197085, 'lng': -95.7080787802915}}}, 'place_id': 'ChIJB9Wh0CLYQIYRoqccrk2qgSU', 'types': ['premise']}, **{"address2": "unit 4"})
    '19311 Cypress Peak Ln, unit 4, Katy, TX 77449, USA'

  """
  formatted_address = geo_result.get("formatted_address")
  parts_list = []
  address_parts = {}
  parts_to_dict = {}
  parsed_address, address_type, extra_parts, detailed_parts, details_score = convert_address_to_parts(formatted_address)
  merged_addresses={}
  recomposed_address = ""
  for (key, value) in parsed_address.items():
    if parts_to_dict.get(key) is None:
      parts_to_dict[key] = value
      part =  parts_to_dict.get(key)
      if part:
        address_parts[key] = part
  
  pre_addresses = {**address_parts, **occupancy_parts}
  parts_labels = MAPPED_TAGS
  for part  in parts_labels:
    current_part = pre_addresses.get(part)
    if pre_addresses.get(part) is not None:
      merged_addresses[part] = current_part

  for (key, value) in merged_addresses.items():
    if parts_to_dict.get(value) is None:
      parts_to_dict[key] = value
      part =  parts_to_dict.get(key)

      if parts_to_dict[key] == parts_to_dict.get("state"):
        part =  part + " "
      elif parts_to_dict[key] == parts_to_dict.get("country"):
        part =  part + ""
      else:
        part =  part + ", "

      if part:
        parts_list.append(part) 
        recomposed_address = "".join(parts_list)
  
  return recomposed_address

def rebuild_address_with_parts(geo_result, **occupancy_parts):
  """
    Reassemble the formatted addressed from the geocoder and the occupancy parts, following the MAPPED_TAGS order 
    
    >>> rebuild_address_string({'address_components': [{'long_name': '19311', 'short_name': '19311', 'types': ['street_number']}, {'long_name': 'Cypress Peak Lane', 'short_name': 'Cypress Peak Ln', 'types': ['route']}, {'long_name': 'Cypress Meadows', 'short_name': 'Cypress Meadows', 'types': ['neighborhood', 'political']}, {'long_name': 'Katy', 'short_name': 'Katy', 'types': ['locality', 'political']}, {'long_name': 'Harris County', 'short_name': 'Harris County', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Texas', 'short_name': 'TX', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'United States', 'short_name': 'US', 'types': ['country', 'political']}, {'long_name': '77449', 'short_name': '77449', 'types': ['postal_code']}, {'long_name': '4102', 'short_name': '4102', 'types': ['postal_code_suffix']}], 'formatted_address': '19311 Cypress Peak Ln, Katy, TX 77449, USA', 'geometry': {'bounds': {'northeast': {'lat': 29.8402578, 'lng': -95.7066444}, 'southwest': {'lat': 29.8400786, 'lng': -95.7068152}}, 'location': {'lat': 29.8401701, 'lng': -95.7067242}, 'location_type': 'ROOFTOP', 'viewport': {'northeast': {'lat': 29.8415171802915, 'lng': -95.7053808197085}, 'southwest': {'lat': 29.8388192197085, 'lng': -95.7080787802915}}}, 'place_id': 'ChIJB9Wh0CLYQIYRoqccrk2qgSU', 'types': ['premise']}, **{"address2": "unit 4"})
    '19311 Cypress Peak Ln, unit 4, Katy, TX 77449, USA'

  """

  address = rebuild_address_string(geo_result, **occupancy_parts)
  address_parts, address_type, extra_parts, detailed_parts, details_score = convert_address_to_parts(address)

  return {
    'address': address,
    'parts': address_parts,
    'type': address_type,
    'extra_parts': extra_parts,
    'detailed_parts': detailed_parts,
    'details_score': details_score,
  }