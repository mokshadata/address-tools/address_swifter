def is_partial_result(address):
  """
    Check if the address contains the field partial_match, returns false otherwise

    >>> is_partial_result([{'address_components': [{'long_name': '12th Street', 'short_name': '12th St', 'types': ['route']}, {'long_name': 'Northwest Houston', 'short_name': 'Northwest Houston', 'types': ['neighborhood', 'political']}, {'long_name': 'Houston', 'short_name': 'Houston', 'types': ['locality', 'political']}, {'long_name': 'Harris County', 'short_name': 'Harris County', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Texas', 'short_name': 'TX', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'United States', 'short_name': 'US', 'types': ['country', 'political']}, {'long_name': '77055', 'short_name': '77055', 'types': ['postal_code']}], 'formatted_address': '12th St, Houston, TX 77055, USA', 'geometry': {'bounds': {'northeast': {'lat': 29.7922347, 'lng': -95.4568133}, 'southwest': {'lat': 29.7907379, 'lng': -95.46090799999999}}, 'location': {'lat': 29.7914797, 'lng': -95.4590194}, 'location_type': 'GEOMETRIC_CENTER', 'viewport': {'northeast': {'lat': 29.7928352802915, 'lng': -95.4568133}, 'southwest': {'lat': 29.7901373197085, 'lng': -95.46090799999999}}}, 'partial_match': True, 'place_id': 'ChIJB6Sgz7_GQIYR3C507RqdZic', 'types': ['route']}])
    True
  """
  for address in address:
      if "partial_match" in address:
        return True
  return False

def is_ambiguous_result(address):
  """
    Check if the address contains the field partial_match, returns false otherwise

    >>> is_ambiguous_result([{"address": "2515 Arapaho Dr, Arlington, TX76018, USA","parts": {"address1": "2515 Arapaho Dr","city": "Arlington","state": "TX76018, USA"},"type": "Street Address","extra_parts": {},"detailed_parts": {"AddressNumber": "2515","StreetName": "Arapaho","StreetNamePostType": "Dr","PlaceName": "Arlington","StateName": "TX76018, USA"}},{"address": "Rosenberg, TX, USA","parts": {"city": "Rosenberg","state": "TX, USA"},"type": "Ambiguous","extra_parts": {},"detailed_parts": {"PlaceName": "Rosenberg","StateName": "TX, USA"}}])
    True
  """
  for address in address:
    for (key, value) in address.items():
      if address.get('type') == "Ambiguous":
        return True
  return False

def handle_flags():
  #todo
  return ""