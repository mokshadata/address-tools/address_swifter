from collections import OrderedDict
from address_swifter.utils import unique

import usaddress

DEFAULT_TAG_MAP = {
    'Recipient': 'recipient',
    'AddressNumber': 'address1',
    'AddressNumberPrefix': 'address1',
    'AddressNumberSuffix': 'address1',
    'StreetName': 'address1',
    'StreetNamePreDirectional': 'address1',
    'StreetNamePreModifier': 'address1',
    'StreetNamePreType': 'address1',
    'StreetNamePostDirectional': 'address1',
    'StreetNamePostModifier': 'address1',
    'StreetNamePostType': 'address1',
    'CornerOf': 'address1',
    'IntersectionSeparator': 'address1',
    'LandmarkName': 'address1',
    'USPSBoxGroupID': 'address1',
    'USPSBoxGroupType': 'address1',
    'USPSBoxID': 'address1',
    'USPSBoxType': 'address1',
    'BuildingName': 'address2',
    'OccupancyType': 'address2',
    'OccupancyIdentifier': 'address2',
    'SubaddressIdentifier': 'address2',
    'SubaddressType': 'address2',
    'PlaceName': 'city',
    'StateName': 'state',
    'ZipCode': 'zip_code',
    'ZipPlus4': 'zip_code',
    'CountryName': 'country'
}

DEFAULT_TAG_SCORING = {
    'Recipient': 0,
    'AddressNumber': 10,
    'AddressNumberPrefix': 1,
    'AddressNumberSuffix': 1,
    'StreetName': 10,
    'StreetNamePreDirectional': 1,
    'StreetNamePreModifier': 1,
    'StreetNamePreType': 1,
    'StreetNamePostDirectional': 1,
    'StreetNamePostModifier': 1,
    'StreetNamePostType': 10,
    'CornerOf': 0,
    'IntersectionSeparator': 0,
    'LandmarkName': 0,
    'USPSBoxGroupID': 0,
    'USPSBoxGroupType': 0,
    'USPSBoxID': 0,
    'USPSBoxType': 0,
    'BuildingName': 0,
    'OccupancyType': 1,
    'OccupancyIdentifier': 1,
    'SubaddressIdentifier': 0,
    'SubaddressType': 0,
    'PlaceName': 10,
    'StateName': 10,
    'ZipCode': 10,
    'ZipPlus4': 1,
    'CountryName': 1
}

DEFAULT_ESSENTIAL_PARTS = {part: score for part, score in DEFAULT_TAG_SCORING.items() if score > 5}

DEFAULT_TOTAL_TAG_SCORE = sum(list(DEFAULT_TAG_SCORING.values()))

ALL_USADDRESS_PARTS = list(DEFAULT_TAG_MAP.keys())
MAPPED_TAGS = unique(DEFAULT_TAG_MAP.values())

def convert_address_to_parts(string):
    """
        Breakdown a string into parts, set address type, return extra parts and detailed tagged address
        >>> convert_address_to_parts("19311 CYPRESS unit 4 PEAK LANE KATY TX USA")
        (OrderedDict([('address1', '19311 CYPRESS'), ('address2', 'unit 4'), ('city', 'PEAK LANE KATY'), ('state', 'TX USA')]), 'Street Address', {}, OrderedDict([('AddressNumber', '19311'), ('StreetName', 'CYPRESS'), ('OccupancyType', 'unit'), ('OccupancyIdentifier', '4'), ('PlaceName', 'PEAK LANE KATY'), ('StateName', 'TX USA')]), 0.6)
    """
    tagged_address, address_type, extra_parts = tag_address(string)
    detailed_tagged_address, detailed_address_type, detailed_extra_parts = tag_address(string, tag_mapping = None)
    # address_completeness = len()
    details = detailed_tagged_address.keys()
    details_score = sum([DEFAULT_TAG_SCORING[detail] for detail in details]) / DEFAULT_TOTAL_TAG_SCORE
    # detailed = {
    #     'tagged_address': detailed_tagged_address,
    #     'address_type': detailed_address_type,
    #     'extra_parts': detailed_extra_parts,
    # }
    return tagged_address, address_type, extra_parts, detailed_tagged_address, details_score


def tag_address(string, tag_mapping = DEFAULT_TAG_MAP):
    """
        Run a string through usaddress and return the parts, address type and extra parts
        >>> tag_address("19311 CYPRESS unit 4 PEAK LANE KATY TX USA")
        (OrderedDict([('address1', '19311 CYPRESS'), ('address2', 'unit 4'), ('city', 'PEAK LANE KATY'), ('state', 'TX USA')]), 'Street Address', {})
    """
    extra_parts = {}
    try:
        tagged_address, address_type = usaddress.tag(string, tag_mapping=tag_mapping)
    except usaddress.RepeatedLabelError as e :
        tagged_address, address_type, extra_parts = handle_repeated_labels(e.parsed_string, e.original_string, tag_mapping=tag_mapping)
    
    return tagged_address, address_type, extra_parts

def handle_repeated_labels(parsed_output, original_string, tag_mapping=None):
    result = OrderedDict()
    parts = OrderedDict()
    extra_parts = OrderedDict()

    prev_parts = []

    for value, key in parsed_output:
        if key in prev_parts:
            # previously found part
            if key == prev_parts[-1]:
                # part is continuing of previous
                parts[key].append(value)
            else:
                # part is a later extra
                if extra_parts.get(key):
                    extra_parts[key].append(value)
                else:
                    extra_parts[key] = [value]
        else:
            prev_parts.append(key)
            parts[key] = [value]

    all_parts = ALL_USADDRESS_PARTS
    if tag_mapping:
        all_parts = list(tag_mapping.keys())

    for part in all_parts:
        if tag_mapping and tag_mapping.get(part):
            label = tag_mapping.get(part)
        else:
            label = part

        if parts.get(part):
            if isinstance(parts.get(part), list):
                component = parts.get(part)
            else:
                component = [parts.get(part)]

            if result.get(label):
                result[label] = result[label] + component
            else:
                result[label] = component

    for token in result: 
        component = ' '.join(result[token])
        component = component.strip(" ,;")
        result[token] = component

    address_type = guess_address_type(parts)

    return result, address_type, extra_parts


# This is ported from https://github.com/datamade/usaddress/blob/master/usaddress/__init__.py#L195-L202
def guess_address_type(address_parts):
    parts = address_parts.keys()

    is_intersection = 'IntersectionSeparator' in parts
    
    if 'AddressNumber' in parts and not is_intersection:
        address_type = 'Street Address'
    elif is_intersection and 'AddressNumber' not in parts:
        address_type = 'Intersection'
    elif 'USPSBoxID' in parts:
        address_type = 'PO Box'
    else:
        address_type = 'Ambiguous'
    return address_type
