import re

def is_empty(address):
  """
    Check if the string is empty
    
    >>> is_empty("")
    True

  """
  is_empty = False
  if not address:
    is_empty = True
  return is_empty

def pre_cleaning_spaces(address):
  """
    Remove spaces at the begining of a line or return address as is
    
    >>> pre_cleaning_spaces("    910 REDWOOD STREET HOUSTON TX USA")
    '910 REDWOOD STREET HOUSTON TX USA'
    >>> pre_cleaning_spaces("910 REDWOOD STREET HOUSTON TX USA")
    '910 REDWOOD STREET HOUSTON TX USA'

  """
  corrected_address = ""
  if re.search(r'(^\s*)', str(address)):
    modified_address = re.sub(r'(^\s*)', '', address)
    corrected_address = modified_address
  else:
    corrected_address = address
  return corrected_address

def missing_street_number(address ):
  """
    Checks if the string starts with alpha characters
    
    >>> missing_street_number("REDWOOD STREET HOUSTON TX USA")
    True

  """
  result = False
  if re.search(r'(^([A-Za-z]))', str(address)):
    result = True
    return result
  return result

def add_space_before_street_name(address):
  """
    Checks if there is a space between the number and the street name then adds a space if needed otherwise, returns string as is
    
    >>> add_space_before_street_name("910REDWOOD STREET HOUSTON TX USA")
    '910 REDWOOD STREET HOUSTON TX USA'
    
    >>> add_space_before_street_name("910 REDWOOD STREET HOUSTON TX USA")
    '910 REDWOOD STREET HOUSTON TX USA'

  """
  address_string = ""
  if re.search(r'(^\d+)(\S)([A-Za-z])', str(address)):
    modified_address = re.sub(r'(?<=\d)(?=[^\d\s])|(?<=[^\d\s])(?=\d)', ' ', address)
    address_string = modified_address
  else:
    address_string = address
  return address_string

DEFAULT_TRANSFORMS = [is_empty, pre_cleaning_spaces, add_space_before_street_name ]

def handle_tranforms(address, transforms):
  """
    Assemble the different transforms and return a cleaned up string

    Stop if string is empty, return nothing
    >>> handle_tranforms("", tuple(['check_empty']))
    

    Stop if the string is missing a street number, return nothing
    >>> handle_tranforms("REDWOOD STREET HOUSTON TX USA", tuple(['check_no_street_number']))


    Remove empty spaces at begining of string
    >>> handle_tranforms("    910 REDWOOD STREET HOUSTON TX USA", tuple(['pre_cleaning_spaces']))
    '910 REDWOOD STREET HOUSTON TX USA'

    Add a space between street number and street name
    >>> handle_tranforms("910REDWOOD STREET HOUSTON TX USA", tuple(['add_space_before_street_name']))
    '910 REDWOOD STREET HOUSTON TX USA'

    Check for empty spaces at the begining of a string and add a space between street number and street name
    >>> handle_tranforms("910REDWOOD STREET HOUSTON TX USA", tuple(['pre_cleaning_spaces', 'add_space_before_street_name']))
    '910 REDWOOD STREET HOUSTON TX USA'

  """
  result = address
  previous_value = False
  removal_report = []
  check_empty = is_empty(address)
  check_no_street_number = missing_street_number(address)

  if check_empty and "check_empty" in transforms:
    removal_report.append("Removed empty")
    return

  if check_no_street_number and "check_no_street_number" in transforms:
    removal_report.append("Removed missing street number")
    return

  if "pre_cleaning_spaces" in transforms: 
    previous_value = True
    result = pre_cleaning_spaces(address)
    
  if "add_space_before_street_name" in transforms:
    if previous_value == True: 
      result = add_space_before_street_name(result)
    else:
      result = add_space_before_street_name(address)

  return result



