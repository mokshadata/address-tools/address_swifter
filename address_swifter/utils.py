import json
from collections import OrderedDict

def quick_dict_serialize(our_dict):
    keys = list(our_dict.keys())
    keys.sort()

    ordered_dict = OrderedDict()
    for key in keys:
        ordered_dict[key] = our_dict.get(key)
    
    return json.dumps(ordered_dict)

def unique(list1):
    # initialize a null list
    unique_list = []
     
    # traverse for all elements
    for x in list1:
        # check if exists in unique_list or not
        if x not in unique_list:
            unique_list.append(x)

    return unique_list