from address_swifter.settings import APIS
from address_swifter.utils import quick_dict_serialize
from cachetools import LRUCache, cached
from cachetools.keys import hashkey

import googlemaps


ALLOWED_GOOGLE_CLIENT_PARAMS = [
    'key',
    'client_id',
    'client_secret',
    'timeout',
    'connect_timeout',
    'read_timeout',
    'retry_timeout',
    'requests_kwargs',
    'queries_per_second',
    'channel',
    'retry_over_query_limit',
    'experience_id',
    'requests_session',
    'base_url',
]

NORTH = 30.166256
EAST = -94.920227
SOUTH = 29.490625
WEST = -95.962188

DEFAULT_BOUNDS = {
    'north': NORTH,
    'south': SOUTH,
    'east': EAST,
    'west': WEST,
}

CACHE = {}

class GoogleMapsWrapper(googlemaps.Client):
    def _get_body(self, response):
        try:
            body = response.json()
            api_status = body["status"]

            if api_status == "OK":
                return body
            
            if api_status == "ZERO_RESULTS":
                return {
                    'error_message': 'No geocode result found.',
                    **body,
                }
            
            return {
                'error_message': f'{body.get("status")}',
                **body,
            }

        except Exception as exception:
            if response.status_code != 200:
                return {
                    'error_message': f'HTTP Error {response.status_code} encountered'
                }
            else:
                return {
                    'error_message': exception
                }

def make_key_for_geocode(*args, bounds = None, geocoder_client_options = {}, **kwargs):
    key = hashkey(*args, **kwargs)
    key += tuple([quick_dict_serialize(bounds or {})])
    return key

@cached(LRUCache(maxsize=128), key=make_key_for_geocode)
def geocode(address_search_string, google_key = None, bounds = None, geocoder_client_options = {}):
    """
    Find geocode address  and return geocoded response object
    
    >>> geocode("1234 Ralph Street Unit 3 Houston TX 77006")
    {'results': [{'address_components': [{'long_name': '3', 'short_name': '3', 'types': ['subpremise']}, {'long_name': '1234', 'short_name': '1234', 'types': ['street_number']}, {'long_name': 'Ralph Street', 'short_name': 'Ralph St', 'types': ['route']}, {'long_name': 'Hyde Park', 'short_name': 'Hyde Park', 'types': ['neighborhood', 'political']}, {'long_name': 'Houston', 'short_name': 'Houston', 'types': ['locality', 'political']}, {'long_name': 'Harris County', 'short_name': 'Harris County', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Texas', 'short_name': 'TX', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'United States', 'short_name': 'US', 'types': ['country', 'political']}, {'long_name': '77006', 'short_name': '77006', 'types': ['postal_code']}], 'formatted_address': '1234 Ralph St #3, Houston, TX 77006, USA', 'geometry': {'location': {'lat': 29.745064, 'lng': -95.40130099999999}, 'location_type': 'RANGE_INTERPOLATED', 'viewport': {'northeast': {'lat': 29.7464129802915, 'lng': -95.39995201970848}, 'southwest': {'lat': 29.7437150197085, 'lng': -95.40264998029149}}}, 'place_id': 'EigxMjM0IFJhbHBoIFN0ICMzLCBIb3VzdG9uLCBUWCA3NzAwNiwgVVNBIiIaIAobEhkKFAoSCYcSrA-ZwECGEciCSLLBiJR1ENIJEgEz', 'types': ['subpremise']}], 'status': 'OK'}
    """
    custom_bounds = bounds or {}
    bound_options = {
        **DEFAULT_BOUNDS,
        **custom_bounds,
    }
    params = {
        'address': address_search_string,
        'bounds': f'{bound_options.get("south")},{bound_options.get("west")}|{bound_options.get("north")},{bound_options.get("east")}',
    }

    key = google_key or APIS.get('google')
    googlemaps_client_options = {
        key: value for key, value in geocoder_client_options.items()
        if key in ALLOWED_GOOGLE_CLIENT_PARAMS
    } or {}
    googlemaps_client_options = {
        **{
            'key': key,
        },
        **googlemaps_client_options,
    }

    if CACHE.get('gmaps_client') is None:
        CACHE['gmaps_client'] = GoogleMapsWrapper(**googlemaps_client_options)

    response = CACHE['gmaps_client']._request("/maps/api/geocode/json", params)
    
    return response

