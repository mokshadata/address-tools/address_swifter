from setuptools import setup, find_packages

install_requires = [
    'usaddress',
    'googlemaps',
    'cachetools'
]

setup(
    name='address_swifter',
    version='0.5.0',
    packages=find_packages(),
    install_requires=install_requires,
)
