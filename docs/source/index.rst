.. Address Tools documentation master file, created by
   sphinx-quickstart on Fri Sep 17 14:20:31 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Address Tools's documentation!
=========================================

.. toctree::
   :maxdepth: 2

   clean_address
   flags
   process_address
   split_to_parts
   transforms
   googlemaps



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
